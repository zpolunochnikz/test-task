<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

use Bitrix\Main\Loader,
	Bitrix\Iblock,
	Bitrix\Iblock\ElementTable,
	Bitrix\Iblock\IblockTable;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if($arParams["IBLOCK_TYPE"] == '')
	$arParams["IBLOCK_TYPE"] = "news";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);

$arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
if($arParams["NEWS_COUNT"] <= 0)
	$arParams["NEWS_COUNT"] = 20;


if($this->startResultCache(false, ($_GET["nav"])))
{
	//Проверка на наличие модуль информационных блоков
	if(!Loader::includeModule("iblock"))
	{
		$this->abortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}


	//Получаем информацию о текущем информационном блоке
	$rsIBlock = IblockTable::getList(array(), array(
		"ID" => $arParams["IBLOCK_ID"],
	));

	//Вытаскиваю элемент из полученного объекта (если ИБ с данным ID есть)
	//Иначе получаю false
	$arResult = $rsIBlock->Fetch();
	if (!$arResult)
	{
		$this->abortResultCache();
		Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_NEWS_NA")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
		return;
	}
	//WHERE
	$arFilter = array (
		"IBLOCK_ID" => $arResult["ID"],
	);

	$nav = new \Bitrix\Main\UI\PageNavigation("nav");
	$nav->setPageSize($arParams["NEWS_COUNT"]);
	//ORDER BY
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"CODE",
		"DETAIL_PAGE_URL"=>'IBLOCK.DETAIL_PAGE_URL',
		"DETAIL_TEXT",
		"PREVIEW_TEXT",
		"PREVIEW_PICTURE",
	);

	$arResult["ITEMS"] = array();
	//Получаю все элементы из данного инфоблока
	// $rsElement =(array("ID"=>"DESC"), $arFilter, false, /*$arNavParams*/ false, $arSelect);

	$rsElement = ElementTable::getList(array(
		'order'=> array("ID"=>"DESC"),
		'select'=>$arSelect,
		'filter'=>$arFilter,
		"count_total" => true,
      	"offset" => $nav->getOffset(),
      	"limit" => $nav->getLimit()
	));


	$nav->setRecordCount($rsElement->getCount());

	while ($arItem = $rsElement->Fetch())
	{
		$id = (int)$arItem['ID'];

		//Добавляю кнопки редактирования/удаления элемента. Не из d7, по этому закомментил

		// $arButtons = CIBlock::GetPanelButtons(
		// 	$arItem["IBLOCK_ID"],
		// 	$arItem["ID"],
		// 	0,
		// 	array("SECTION_BUTTONS" => false, "SESSID" => false)
		// );
		// $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		// $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

		$arItem['DETAIL_PAGE_URL'] = CIBlock::ReplaceDetailUrl($arItem['DETAIL_PAGE_URL'], $arItem, false, 'E');

		Iblock\Component\Tools::getFieldImageData(
			$arItem,
			array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
			Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT
		);

		$arResult["ITEMS"][$id] = $arItem;
	}
	unset($row);

	$arResult["nav"] = $nav;



	$this->setResultCacheKeys(array(
		"nav"
	));
	$this->includeComponentTemplate();
}
?>
<?
?>