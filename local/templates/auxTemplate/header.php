<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php $APPLICATION->ShowTitle(); ?></title>
		<?php
			$APPLICATION->ShowPanel();
			$APPLICATION->ShowHead();
		?>
	</head>
	<body>